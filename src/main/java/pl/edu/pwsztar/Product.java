package pl.edu.pwsztar;

public class Product {
    String productName;
    int price;
    int quantity;

    public Product(String productName, int price, int quantity) {
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean addQuantity(int value){
        if (value <= 0){
            return false;
        } else {
            quantity += value;
            return true;
        }
    }

    public boolean removeQuantity(int value){
        if (value <= 0) {
            return false;
        }

        int changedQuantity = quantity - value;
        if (changedQuantity < 0){
            return false;
        }else{
            quantity = changedQuantity;
            return true;
        }
    }
}
