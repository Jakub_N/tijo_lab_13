package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {

    List<Product> products = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        if(getAllProductsAmount() + quantity > PRODUCTS_LIMIT){
            return false;
        }

        if(price <= 0 || quantity <= 0){
            return false;
        }

        for(Product product : products){
            if (productName.equals(product.getProductName())){
                if(price == product.getPrice()){
                    return product.addQuantity(quantity);
                }else{
                    return false;
                }
            }
        }

        //Create new if don't exist
        Product product = new Product(productName,price,quantity);
        products.add(product);
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(quantity <= 0){
            return false;
        }

        for (Product product : products){
            if (productName.equals(product.getProductName())){
                boolean result = product.removeQuantity(quantity);
                if(product.getQuantity() == 0){
                    products.remove(product);
                }

                return result;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        for (Product product : products){
            if (productName.equals(product.getProductName())){
                return product.getQuantity();
            }
        }
        return 0;
    }

    public int getSumProductsPrices() {
        int totalAmount = 0;

        for (Product product : products){
            int totalProductAmount = product.getQuantity() * product.getPrice();
            totalAmount += totalProductAmount;
        }
        return totalAmount;
    }

    public int getProductPrice(String productName) {
        for (Product product : products){
            if(productName.equals(product.getProductName())){
                return product.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        List<String> productNames = new ArrayList<>();

        for (Product product : products){
            productNames.add(product.getProductName());
        }
        return productNames;
    }

    public int getAllProductsAmount(){
        int totalAmount = 0;
        for (Product product : products){
            totalAmount += product.getQuantity();
        }

        return totalAmount;
    }
}
